'''
Created on Jun 28, 2012

@author: gmcgregor
'''


from tanto.tanto import TantoParser

from logging import info, debug, warning
from mako.template import Template
 
class DummyParser(TantoParser):
    
    def parse(self):
        '''Test parser'''
        debug('Returning a dummy data structure ' )
        
        return { 'registers': ('ID', 'INDEX'),
                       'register_info':
                            {'ID':   {'fields':('REVISION_ID', 'CHIP_ID', 'PRODUCT_ID')} ,
                             'INDEX':{'fields':('value',)}
                             }
                      }

